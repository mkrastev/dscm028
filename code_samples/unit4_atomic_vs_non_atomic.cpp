// control macros
// EVEN_START  -- all workers start at a barrier; default is don't
// ATOMIC_FLAG -- done-work flag is atomic; default is volatile
// ACTUAL_WORK -- doer does some time-consuming work; default is don't
// INTROSPECTION -- print worker affinity info

#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <sched.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <atomic>

// we place key data in separate cachelines for maximum impact of weak-memory-ordering
#define CACHE_LINE_SIZE	64

pthread_barrier_t barrier; // even-start barrier

#if ATOMIC_FLAG
#warning atomic flag
// atomic version of the done-work flag: compiler applies acquire-release semantics or data-memory barriers around access
std::atomic<bool> flag; // done-work flag

#else
#warning volatile flag
// volatile version of the done-work flag: compiler makes sure to read the flag from memory upon every
// access but does nothing architectural to ensure visibility
volatile bool flag __attribute__ ((aligned(CACHE_LINE_SIZE))); // done-work flag

#endif
pthread_mutex_t dmutex __attribute__ ((aligned(CACHE_LINE_SIZE))) = PTHREAD_MUTEX_INITIALIZER; // critical-section guard
pthread_t doer __attribute__ ((aligned(CACHE_LINE_SIZE))); // thread that did the work
uint32_t ndoers; // how many threads did the work (should always be 1)

void* worker(void*)
{
	const pthread_t self = pthread_self();

#if INTROSPECTION
	#warning introspection
	pthread_attr_t attr;
	int err = 0;

	// thread introspection
	err = pthread_getattr_np(self, &attr);
	if (err < 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);

	err = pthread_attr_getaffinity_np(&attr, sizeof(cpuset), &cpuset);
	if (err != 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	err = pthread_attr_destroy(&attr);
	if (err != 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	size_t count = CPU_COUNT(&cpuset);
	for (size_t i = 0; i < size_t(CPU_SETSIZE) && count; ++i) {
		if (CPU_ISSET(i, &cpuset)) {
			fprintf(stderr, "%lx: cpu %lu\n", self, i);
			count -= 1;
		}
	}

#if EVEN_START
#warning even start
	err = pthread_barrier_wait(&barrier);
	if (err != 0 && err != PTHREAD_BARRIER_SERIAL_THREAD) {
		fprintf(stderr, "error: %p: %s, %d, %s\n", self, __FUNCTION__, __LINE__, strerror(err));
	}

#endif
#endif
	if (!flag) {
		pthread_mutex_lock(&dmutex);
		if (!flag) {
			// work
#if ACTUAL_WORK
#warning actual work
			for (size_t i = 0; i < size_t(1e9); ++i)
				asm volatile ("" ::: "memory");

#endif
			doer = self;
			ndoers++;
			flag = true;
		}
		else {
			fprintf(stderr, "%lx: inner check\n", self);
		}
		pthread_mutex_unlock(&dmutex);
	}

	assert(doer);
	return nullptr;
}

int main(int, char**)
{
	pthread_attr_t attr;
	int err = 0;

	err = pthread_attr_init(&attr);
	if (err < 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	const size_t nworkers = N_WORKERS;
	pthread_t workers[nworkers] = {};

	err = pthread_barrier_init(&barrier, nullptr, nworkers);
	if (err != 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	for (size_t i = 0; i < nworkers; ++i) {
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(i, &cpuset);

		err = pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset);
		if (err != 0) {
			fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
		}

		err = pthread_create(workers + i, &attr, worker, nullptr);
		if (err != 0) {
			workers[i] = 0;
			fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
		}
	}

	err = pthread_attr_destroy(&attr);
	if (err != 0) {
		fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
	}

	for (size_t i = 0; i < nworkers; ++i) {
		if (workers[i]) {
			err = pthread_join(workers[i], nullptr);
			if (err != 0) {
				fprintf(stderr, "error: %s, %d, %s\n", __FUNCTION__, __LINE__, strerror(err));
			}
		}
	}

	printf("doer: %lx, num doers: %u\n", doer, ndoers);
	assert(1 == ndoers);
	return 0;
}

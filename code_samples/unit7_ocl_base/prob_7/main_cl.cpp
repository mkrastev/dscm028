#include <CL/cl.h>
#if CL_VERSION_1_2 == 0
	#error required CL_VERSION_1_2 to compile
#endif
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include "timer.h"
#include "pure_macro.hpp"
#include "cl_util.hpp"
#include "cl_wrap.hpp"
#include <png.h>
#ifndef Z_BEST_COMPRESSION
#define Z_BEST_COMPRESSION 9
#endif
#include "get_file_size.hpp"
#include "scoped.hpp"
#include "stream.hpp"

// verify iostream-free status
#if _GLIBCXX_IOSTREAM
#error rogue iostream acquired
#endif

namespace stream {

// deferred initialization by main()
in cin;
out cout;
out cerr;

} // namespace stream

const char arg_prefix[]                   = "-";
const char arg_report_caps[]              = "report_caps";
const char arg_discard_platform_version[] = "discard_platform_version";
const char arg_discard_device_version[]   = "discard_device_version";
const char arg_platform[]                 = "platform";
const char arg_device[]                   = "device";
const char arg_report_kernel_time[]       = "report_kernel_time";
const char arg_screen[]                   = "screen";
const char arg_frames[]                   = "frames";

const size_t n_buffering = 2;

namespace testbed {

template <>
class scoped_functor< cl_context > {
public:
	void operator()(cl_context* arg) {
		assert(0 != arg);
		if (cl_context(0) != *arg)
			clReleaseContext(*arg);
	}
};

template <>
class scoped_functor< cl_command_queue > {
public:
	void operator()(cl_command_queue* arg) {
		assert(0 != arg);
		if (cl_command_queue(0) != *arg)
			clReleaseCommandQueue(*arg);
	}
};

template <>
class scoped_functor< cl_mem > {
public:
	void operator()(cl_mem* arg) {
		assert(0 != arg);
		for (size_t i = 0; i < n_buffering; ++i)
			if (cl_mem(0) != arg[i])
				clReleaseMemObject(arg[i]);
	}
};

template <>
class scoped_functor< cl_program > {
public:
	void operator()(cl_program* arg) {
		assert(0 != arg);
		if (cl_program(0) != *arg)
			clReleaseProgram(*arg);
	}
};

template <>
class scoped_functor< cl_kernel > {
public:
	void operator ()(cl_kernel* arg) {
		assert(0 != arg);
		if (cl_kernel(0) != *arg)
			clReleaseKernel(*arg);
	}
};

template < typename T >
class generic_free {
public:
	void operator()(T* arg) {
		assert(0 != arg);
		std::free(arg);
	}
};

} // namespace testbed

namespace testbed {

template <>
class scoped_functor< FILE > {
public:
	void operator()(FILE* arg) {
		assert(0 != arg);
		fclose(arg);
	}
};

} // namespace testbed

template < bool >
struct compile_assert;

template <>
struct compile_assert< true > {
    compile_assert() {}
};

static bool
write_png(
	const bool grayscale,
	const unsigned w,
	const unsigned h,
	void* const bits,
	FILE* const fp)
{
	using testbed::scoped_ptr;
	using testbed::generic_free;

	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (!png_ptr)
		return false;

	png_infop info_ptr = png_create_info_struct(png_ptr);

	if (!info_ptr) {
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		return false;
	}

	// declare any RAII before the longjump, lest no destruction at longjump
	const scoped_ptr< png_bytep, generic_free > row((png_bytepp) malloc(sizeof(png_bytep) * h));

	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return false;
	}

	size_t pixel_size = sizeof(png_byte[3]);
	int color_type = PNG_COLOR_TYPE_RGB;

	if (grayscale) {
		pixel_size = sizeof(png_byte);
		color_type = PNG_COLOR_TYPE_GRAY;
	}

	for (size_t i = 0; i < h; ++i)
		row()[i] = (png_bytep) bits + w * (h - 1 - i) * pixel_size;

	png_init_io(png_ptr, fp);
	png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
	png_set_IHDR(png_ptr, info_ptr, w, h, 8, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	png_set_rows(png_ptr, info_ptr, row());
	png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	png_destroy_write_struct(&png_ptr, &info_ptr);
	return true;
}

static bool validate_fullscreen(
	const char* const string,
	unsigned& screen_w,
	unsigned& screen_h) {

	if (0 == string)
		return false;

	unsigned x = 0;
	unsigned y = 0;

	if (2 != sscanf(string, "%u %u", &x, &y))
		return false;

	if (!x || !y)
		return false;

	screen_w = x;
	screen_h = y;

	return true;
}

struct cli_param {
	enum {
		BIT_REPORT_CAPS              = 1,
		BIT_DISCARD_PLATFORM_VERSION = 2,
		BIT_DISCARD_DEVICE_VERSION   = 4,
		BIT_REPORT_KERNEL_TIME       = 8
	};

	unsigned platform_idx;
	unsigned device_idx;
	unsigned flags;

	unsigned image_w;
	unsigned image_h;
	unsigned frames;
};

static int
parse_cli(
	const int argc,
	char** const argv,
	cli_param& param) {

	const size_t prefix_len = std::strlen(arg_prefix);
	bool success = true;

	for (int i = 1; i < argc && success; ++i) {
		if (std::strncmp(argv[i], arg_prefix, prefix_len)) {
			success = false;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_report_caps)) {
			param.flags |= cli_param::BIT_REPORT_CAPS;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_platform_version)) {
			param.flags |= cli_param::BIT_DISCARD_PLATFORM_VERSION;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_device_version)) {
			param.flags |= cli_param::BIT_DISCARD_DEVICE_VERSION;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_platform)) {
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.platform_idx))
				success = false;

			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_device)) {
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.device_idx))
				success = false;

			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_report_kernel_time)) {
			param.flags |= cli_param::BIT_REPORT_KERNEL_TIME;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_screen)) {
			if (++i == argc || !validate_fullscreen(argv[i], param.image_w, param.image_h))
				success = false;

			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_frames)) {
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.frames))
				success = false;

			continue;
		}

		success = false;
	}

	if (!success) {
		stream::cerr << "usage: " << argv[0] << " [<option> ...]\n"
			"options (multiple args to an option must constitute a single string, eg. -foo \"a b c\"):\n"
			"\t" << arg_prefix << arg_report_caps << "\t\t\t: report CL capabilities\n"
			"\t" << arg_prefix << arg_discard_platform_version << "\t: discard advertised platform version when producing platform report\n"
			"\t" << arg_prefix << arg_discard_device_version << "\t\t: discard advertised device version when producing device report\n"
			"\t" << arg_prefix << arg_platform << " <index>\t\t: use platform of specified index\n"
			"\t" << arg_prefix << arg_device << " <index>\t\t\t: use device of specified index\n"
			"\t" << arg_prefix << arg_report_kernel_time << "\t\t: report CL kernel time\n"
			"\t" << arg_prefix << arg_screen << " <width> <height>\t: set screen output of specified geometry\n"
			"\t" << arg_prefix << arg_frames << " <count>\t\t\t: set number of frames to run; default is max unsigned int\n";

		return 1;
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// the global control state
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {

	using testbed::scoped_linkage_ptr;
	using testbed::scoped_ptr;
	using testbed::scoped_functor;
	using testbed::generic_free;
	using testbed::deinit_resources_t;
	using testbed::get_buffer_from_file;

	using clutil::ocl_ver;
	using clutil::cldevice_version;
	using clutil::cldevice_type_single_string;
	using clutil::reportCLError;
	using clutil::reportCLCaps;

	stream::cin.open(stdin);
	stream::cout.open(stdout);
	stream::cerr.open(stderr);

	cli_param param;
	param.platform_idx = 0;
	param.device_idx = -1U;
	param.flags = 0;
	param.image_w = 512;
	param.image_h = 512;
	param.frames = -1U;

	const int result_cli = parse_cli(argc, argv, param);

	if (0 != result_cli)
		return result_cli;

	const size_t image_w = param.image_w;
	const size_t image_h = param.image_h;
	const unsigned frames = param.frames;

	// opencl control args ////////////////////////////////////////////////////////
	const bool report_caps                  = bool(param.flags & cli_param::BIT_REPORT_CAPS);
	const bool report_kernel_time           = bool(param.flags & cli_param::BIT_REPORT_KERNEL_TIME);
	const bool discard_platform_version     = bool(param.flags & cli_param::BIT_DISCARD_PLATFORM_VERSION);
	const bool discard_device_version       = bool(param.flags & cli_param::BIT_DISCARD_DEVICE_VERSION);
	const unsigned platform_idx             = param.platform_idx;
	unsigned device_idx                     = param.device_idx;

	if (report_caps) {
		const int result_caps = reportCLCaps(discard_platform_version, discard_device_version);

		if (0 != result_caps)
			return result_caps;
	}

	cl_uint num_platforms = 0;
	cl_int success = clGetPlatformIDs(num_platforms, 0, &num_platforms);

	if (reportCLError(success)) {
		stream::cerr << "failure at clGetPlatformIDs; terminate\n";
		return -1;
	}

	if (platform_idx >= unsigned(num_platforms)) {
		stream::cerr << "requested platform index is out of bounds\n";
		return 1;
	}

	const scoped_ptr< cl_platform_id, generic_free > platform(
		reinterpret_cast< cl_platform_id* >(std::malloc(sizeof(cl_platform_id) * num_platforms)));

	success = clGetPlatformIDs(num_platforms, platform(), 0);

	if (reportCLError(success)) {
		stream::cerr << "failure at clGetPlatformIDs; terminate\n";
		return -1;
	}

	scoped_ptr< cl_device_id, generic_free > device;

	if (-1U != device_idx) {
		cl_uint num_devices = 0;

		success = clGetDeviceIDs(platform()[platform_idx],
			CL_DEVICE_TYPE_ALL, num_devices, 0, &num_devices);

		if (reportCLError(success)) {
			stream::cerr << "failure at clGetDeviceIDs; terminate\n";
			return -1;
		}

		if (device_idx >= unsigned(num_devices)) {
			stream::cerr << "requested device index is out of bound\n";
			return 1;
		}

		scoped_ptr< cl_device_id, generic_free > proto_device(
			reinterpret_cast< cl_device_id* >(std::malloc(sizeof(cl_device_id) * num_devices)));

		success = clGetDeviceIDs(platform()[platform_idx],
			CL_DEVICE_TYPE_ALL, num_devices, proto_device(), 0);

		if (reportCLError(success)) {
			stream::cerr << "failure at clGetDeviceIDs; terminate\n";
			return -1;
		}

		device.swap(proto_device);
	}
	else {
		const cl_device_type devtype = CL_DEVICE_TYPE_GPU;

		scoped_ptr< cl_device_id, generic_free > proto_device(
			reinterpret_cast< cl_device_id* >(std::malloc(sizeof(cl_device_id))));

		success = clGetDeviceIDs(platform()[platform_idx],
			devtype, 1, proto_device(), 0);

		if (reportCLError(success)) {
			stream::cerr << "error getting device of type " <<
				cldevice_type_single_string(devtype) << "; use " << arg_prefix << arg_device << " to specify a device.\n";
			return -1;
		}

		device_idx = 0;
		device.swap(proto_device);
	}

	ocl_ver platform_version, device_version;

	if (!clplatform_version(platform()[platform_idx], platform_version) ||
		!cldevice_version(device()[device_idx], device_version)) {

		stream::cerr << "error getting platform/device version\n";
		return -1;
	}

	if (!clwrap::init(platform_version)) {
		stream::cerr << "error initializing API wrappers\n";
		return -1;
	}

	const cl_context_properties prop[] = {
		CL_CONTEXT_PLATFORM, cl_context_properties(platform()[platform_idx]),
		0
	};

	cl_context context = clCreateContext(prop, 1, device() + device_idx, 0, 0, &success);

	if (reportCLError(success)) {
		stream::cerr << "error creating context\n";
		return -1;
	}

	const scoped_ptr< cl_context, scoped_functor > release_context(&context);

	cl_command_queue queue = clCreateCommandQueue(context,
		device()[device_idx], CL_QUEUE_PROFILING_ENABLE, &success);

	if (reportCLError(success)) {
		stream::cerr << "error creating command queue\n";
		return -1;
	}

	const scoped_ptr< cl_command_queue, scoped_functor > release_queue(&queue);

	// output map /////////////////////////////////////////////////////////////////
	const size_t mem_size_image = image_w * image_h * sizeof(cl_uchar[3]);

	const scoped_ptr< cl_uchar, generic_free > image_map(
		reinterpret_cast< cl_uchar* >(std::malloc(mem_size_image * n_buffering)));

	if (0 == image_map()) {
		stream::cerr << "error allocating image_map\n";
		return -1;
	}

	cl_uchar* image_map_buffer[n_buffering];

	for (size_t i = 0; i < COUNT_OF(image_map_buffer); ++i)
		image_map_buffer[i] = image_map() + i * mem_size_image;

	cl_mem_flags dst_flags = CL_MEM_WRITE_ONLY;

	if (device_version >= ocl_ver(1, 2))
		dst_flags |= CL_MEM_HOST_READ_ONLY;

	cl_mem dst_d[n_buffering] = { 0 };

	const scoped_ptr< cl_mem, scoped_functor > release_dst(dst_d);

	for (size_t i = 0; i < COUNT_OF(dst_d); ++i) {
		dst_d[i] = clCreateBuffer(context, dst_flags, mem_size_image, 0, &success);

		if (reportCLError(success)) {
			stream::cerr << "error creating buffer for result\n";
			return -1;
		}
	}

	// kernel sources /////////////////////////////////////////////////////////////
	size_t source_main_len = 0;

	const scoped_ptr< char, generic_free > source_main(get_buffer_from_file("kernel/main.cl", source_main_len));
	if (!source_main_len) {
		stream::cerr << "error: cannot read main.cl\n";
		return -1;
	}

	const char* source[] = {
		source_main()
	};
	size_t length[] = {
		source_main_len
	};

	cl_program program = clCreateProgramWithSource(context,
		COUNT_OF(source), source, length, &success);

	if (reportCLError(success)) {
		stream::cerr << "error creating program with source\n";
		return -1;
	}

	const scoped_ptr< cl_program, scoped_functor > release_program(&program);
	const char build_opt[] =
		"-cl-mad-enable -cl-fast-relaxed-math -D SOME_MACRO";
	success = clBuildProgram(program, 1, device() + device_idx, build_opt, 0, 0);

	if (reportCLError(success) && CL_BUILD_PROGRAM_FAILURE != success) {
		stream::cerr << "error building program\n";
		return -1;
	}

	cl_build_status build_status = CL_BUILD_NONE;
	success = clGetProgramBuildInfo(program,
		device()[device_idx], CL_PROGRAM_BUILD_STATUS, sizeof(build_status), &build_status, 0);

	if (reportCLError(success)) {
		stream::cerr << "error getting build info (build_status)\n";
		return -1;
	}

	if (CL_BUILD_SUCCESS != build_status || OCL_KERNEL_BUILD_VERBOSE) {
		size_t log_len = 0;
		success = clGetProgramBuildInfo(program,
			device()[device_idx], CL_PROGRAM_BUILD_LOG, 0, 0, &log_len);

		if (reportCLError(success)) {
			stream::cerr << "error getting build info (log_len)\n";
			return -1;
		}

		const scoped_ptr< char, generic_free > build_log(
			reinterpret_cast< char* >(std::calloc(log_len, sizeof(char))));

		success = clGetProgramBuildInfo(program,
			device()[device_idx], CL_PROGRAM_BUILD_LOG, log_len, build_log(), 0);

		if (reportCLError(success)) {
			stream::cerr << "error getting build info (build_log)\n";
			return -1;
		}

		stream::cerr << build_log() << '\n';

		if (CL_BUILD_SUCCESS != build_status)
			return -1;
	}

	cl_kernel kernel = clCreateKernel(program, "monokernel", &success);

	if (reportCLError(success)) {
		stream::cerr << "error creating kernel\n";
		return -1;
	}

	const scoped_ptr< cl_kernel, scoped_functor > release_kernel(&kernel);

	size_t local_ws_multiple = 0;
	success = clGetKernelWorkGroupInfo(kernel,
		device()[device_idx], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
		sizeof(local_ws_multiple), &local_ws_multiple, 0);

	if (reportCLError(success)) {
		stream::cerr << "error getting kernel preferred workgroup size multiple info\n";
		return -1;
	}

	stream::cout << "kernel preferred workgroup size multiple: " << local_ws_multiple << '\n';

	cl_uint max_dim = 0;
	success = clGetDeviceInfo(
		device()[device_idx], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
		sizeof(max_dim), &max_dim, 0);

	if (reportCLError(success)) {
		stream::cerr << "error getting device max work-item dimensions info\n";
		return -1;
	}

	size_t buffer_devinfo[8];

	if (max_dim > COUNT_OF(buffer_devinfo)) {
		stream::cerr << "device max work-item dimensions exceed expectations; bailing out\n";
		return -1;
	}

	success = clGetDeviceInfo(
		device()[device_idx], CL_DEVICE_MAX_WORK_ITEM_SIZES,
		sizeof(buffer_devinfo), buffer_devinfo, 0);

	if (reportCLError(success)) {
		stream::cerr << "error getting device max work-item sizes info\n";
		return -1;
	}

	stream::cout << "device max work-item sizes:";
	for (cl_uint i = 0; i < max_dim; ++i)
		stream::cout << ' ' << buffer_devinfo[i];
	stream::cout << '\n';

	const size_t work_item_size_0 = buffer_devinfo[0];
	const size_t work_item_size_1 = buffer_devinfo[1];

	success = clGetDeviceInfo(
		device()[device_idx], CL_DEVICE_MAX_WORK_GROUP_SIZE,
		sizeof(buffer_devinfo), buffer_devinfo, 0);

	if (reportCLError(success)) {
		stream::cerr << "error getting device max work-group size info\n";
		return -1;
	}

	// stochastics to chose a workgroup size
	const size_t max_local_ws = buffer_devinfo[0];
	const size_t latency_hiding_factor = local_ws_multiple * 2 > max_local_ws ? 1 : 2;
	const size_t combined_item_size_0 = local_ws_multiple * latency_hiding_factor;
	const size_t global_ws[] = { image_w, image_h };
	const cl_uint work_dim = COUNT_OF(global_ws);
	size_t local_ws[work_dim];

	if (latency_hiding_factor > work_item_size_1) {
		if (combined_item_size_0 > work_item_size_0) {
			local_ws[0] = work_item_size_0;
			local_ws[1] = 1;
		}
		else {
			local_ws[0] = combined_item_size_0;
			local_ws[1] = 1;
		}
	}
	else {
		local_ws[0] = local_ws_multiple;
		local_ws[1] = latency_hiding_factor;
	}

	for (cl_uint i = 0; i < work_dim; ++i)
		if (0 != global_ws[i] % local_ws[i]) {
			stream::cerr << "ND range not a multiple of workgroup size; bailing out\n";
			return 1;
		}

	const compile_assert< (n_buffering & n_buffering - 1) == 0 > assert_pot_buffering;

	cl_event event_kernel_complete[n_buffering] = { 0 };
	cl_event event_buffer_complete[n_buffering] = { 0 };

	size_t frame = 0;
	const uint64_t t0 = timer_ns();
	float accum_time = 0.f;

	while (frame != size_t(frames)) {
		const uint64_t tframe = timer_ns();
		const float dt = 1.0 / FRAME_RATE;

		const size_t nbuf_idx = frame & n_buffering - 1;

		// launch kernel on new frame data set
		success = clSetKernelArg(kernel, 0, sizeof(dst_d[0]), &dst_d[nbuf_idx]);

		if (reportCLError(success)) {
			stream::cerr << "error setting kernel arg 0\n";
			break;
		}

		success = clSetKernelArg(kernel, 1, sizeof(accum_time), &accum_time);

		if (reportCLError(success)) {
			stream::cerr << "error setting kernel arg 1\n";
			break;
		}

		// first n frames non-withstanding, don't run the kernel until the corresponding output buffer is copied to host
		if (n_buffering > frame) {
			success = clEnqueueNDRangeKernel(queue, kernel, work_dim, 0, global_ws, local_ws,
				0, 0, &event_kernel_complete[nbuf_idx]);
		}
		else {
			success = clEnqueueNDRangeKernel(queue, kernel, work_dim, 0, global_ws, local_ws,
				1, &event_buffer_complete[nbuf_idx], &event_kernel_complete[nbuf_idx]);
		}

		if (reportCLError(success)) {
			stream::cerr << "error enqueuing kernel\n";
			break;
		}

		success = clEnqueueReadBuffer(queue, dst_d[nbuf_idx], CL_TRUE, 0, mem_size_image, image_map_buffer[nbuf_idx],
			1, &event_kernel_complete[nbuf_idx], &event_buffer_complete[nbuf_idx]);

		if (reportCLError(success)) {
			stream::cerr << "error enqueuing buffer read\n";
			break;
		}

		// profile the frame
		if (report_kernel_time) {
			cl_ulong t0, t1;

			success = clGetEventProfilingInfo(
				event_kernel_complete[nbuf_idx], CL_PROFILING_COMMAND_START, sizeof(t0), &t0, 0);

			if (reportCLError(success)) {
				stream::cerr << "error getting profiling info (command_start)\n";
				break;
			}

			success = clGetEventProfilingInfo(
				event_kernel_complete[nbuf_idx], CL_PROFILING_COMMAND_END, sizeof(t1), &t1, 0);

			if (reportCLError(success)) {
				stream::cerr << "error getting profiling info (command_end)\n";
				break;
			}

			stream::cout << "elapsed time (ns): " << t1 - t0 << '\n';
		}

		// upate run time (we aren't supposed to run long - fp32 should do)
		accum_time += dt;

		++frame;
	}

	const size_t nbuf_idx = frame - 1 & n_buffering - 1;

	// wait for the last buffer copy to finish before accessing the corresponding buffer
	if (frame) {
		success = clWaitForEvents(1, &event_buffer_complete[nbuf_idx]);

		if (reportCLError(success)) {
			stream::cerr << "error waiting for ocl done\n";
			return -1;
		}
	}

	const uint64_t sequence_dt = timer_ns() - t0;

	stream::cout << "total frames rendered: " << frame << '\n';

	if (sequence_dt) {
		const double sec = double(sequence_dt) * 1e-9;
		stream::cout << "elapsed time: " << sec << " s\naverage FPS: " << frame / sec << '\n';
	}

	if (frame) {
		const char* const name = "last_frame.png";
		stream::cout << "saving framegrab as '" << name << "'\n";
		const testbed::scoped_ptr< FILE, testbed::scoped_functor > file(fopen(name, "wb"));

		if (0 == file()) {
			stream::cerr << "failure opening framegrab file '" << name << "'\n";
			return -1;
		}

		if (!write_png(false, image_w, image_h, image_map_buffer[nbuf_idx], file())) {
			stream::cerr << "failure writing framegrab file '" << name << "'\n";
			return -1;
		}
	}

	return 0;
}

__kernel
void monokernel(
	__global uchar* const dst,
	float accum_time)
{
	// short-name aliases for work-item coordinates
	const int idx = (int)get_global_id(0);
	const int idy = (int)get_global_id(1);
	const int dimx = (int)get_global_size(0);
	const int dimy = (int)get_global_size(1);

	// checker pattern based on idx/idy coords
	const uchar3 res = ((idx ^ idy) & 0x40) ? (uchar3)(255, 0, 0) : (uchar3)(0, 0, 255);

	// output RGB pixel to (idy, idx) coords
	dst[(dimx * idy + idx) * sizeof(uchar[3]) + 0] = res.x;
	dst[(dimx * idy + idx) * sizeof(uchar[3]) + 1] = res.y;
	dst[(dimx * idy + idx) * sizeof(uchar[3]) + 2] = res.z;
}

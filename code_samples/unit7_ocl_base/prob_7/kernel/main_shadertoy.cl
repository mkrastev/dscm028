/**
 * Direct GLSL-to-OpenCL-C translation of https://www.shadertoy.com/view/llt3R4
 */
typedef float2 vec2;
typedef float3 vec3;
typedef float4 vec4;

__constant int MAX_MARCHING_STEPS = 255;
__constant float MIN_DIST = 0.0;
__constant float MAX_DIST = 100.0;
__constant float EPSILON = 0.0001;

/**
 * Signed distance function for a sphere centered at the origin with radius 1.0;
 */
float sphereSDF(vec3 samplePoint) {
    return length(samplePoint) - 1.f;
}

/**
 * Signed distance function describing the scene.
 *
 * Absolute value of the return value indicates the distance to the surface.
 * Sign indicates whether the point is inside or outside the surface,
 * negative indicating inside.
 */
float sceneSDF(vec3 samplePoint) {
    return sphereSDF(samplePoint);
}

/**
 * Return the shortest distance from the eyepoint to the scene surface along
 * the marching direction. If no part of the surface is found between start and end,
 * return end.
 *
 * eye: the eye point, acting as the origin of the ray
 * marchingDirection: the normalized direction to march in
 * start: the starting distance away from the eye
 * end: the max distance away from the ey to march before giving up
 */
float shortestDistanceToSurface(vec3 eye, vec3 marchingDirection, float start, float end) {
    float depth = start;
    for (int i = 0; i < MAX_MARCHING_STEPS; i++) {
        float dist = sceneSDF(eye + depth * marchingDirection);
        if (dist < EPSILON) {
            return depth;
        }
        depth += dist;
        if (depth >= end) {
            return end;
        }
    }
    return end;
}


/**
 * Return the normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * size: resolution of the output image
 * fragCoord: the x,y coordinate of the pixel in the output image
 */
vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.f;
    float z = size.y / tan(radians(fieldOfView) / 2.f);
    return normalize((vec3)(xy, -z));
}


vec3 mainImage(vec2 iResolution, vec2 fragCoord)
{
    vec3 dir = rayDirection(45.f, iResolution.xy, fragCoord);
    vec3 eye = (vec3)(0.f, 0.f, 5.f);
    float dist = shortestDistanceToSurface(eye, dir, MIN_DIST, MAX_DIST);

    if (dist > MAX_DIST - EPSILON) {
        // Didn't hit anything
        return (vec3)(0.f, 0.f, 0.f);
    }

    return (vec3)(1.f, 0.f, 0.f);
}

//
// Translation ends here; below is genuine OpenCL-C code
//

__kernel
void monokernel(
	__global uchar* const dst,
	float accum_time)
{
	const vec2 iResolution = (vec2)(get_global_size(0), get_global_size(1));
	const vec2 fragCoord = (vec2)(get_global_id(0), get_global_id(1));
	const uchar3 res = convert_uchar3(mainImage(iResolution, fragCoord) * 255);

	// Output uchar3 RGB pixel to [get_global_id(1)][get_global_id(0)] coords
	dst[(get_global_size(0) * get_global_id(1) + get_global_id(0)) * sizeof(uchar[3]) + 0] = res.x;
	dst[(get_global_size(0) * get_global_id(1) + get_global_id(0)) * sizeof(uchar[3]) + 1] = res.y;
	dst[(get_global_size(0) * get_global_id(1) + get_global_id(0)) * sizeof(uchar[3]) + 2] = res.z;
}

#include <stdint.h>
#include <pthread.h>

int32_t the_answer;
int32_t is_ready;
pthread_rwlock_t lock = PTHREAD_RWLOCK_INITIALIZER;

void __attribute__ ((noinline)) deep_thought_compute()
{
	pthread_rwlock_wrlock(&lock);
	the_answer = 42;
	is_ready = 1;
	pthread_rwlock_unlock(&lock);
}

int main(int, char**)
{
	// thread 1
	deep_thought_compute();

	// thread 2
	bool is_ready_copy;
	do {
		pthread_rwlock_rdlock(&lock);
		is_ready_copy = is_ready;
		pthread_rwlock_unlock(&lock);
	}
	while (!is_ready_copy);

	return the_answer;
}

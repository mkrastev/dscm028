#if __SSE4_1__ != 0
	#include <smmintrin.h>
#else
	#include <emmintrin.h>
#endif
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include "timer.h"

float input[64] __attribute__ ((aligned(64)));
float output[64] __attribute__ ((aligned(64)));


static void odd_even_scalar_sort(
	float (& inout)[8]) {

	const size_t idx[][2] = {
		{ 0, 1 }, { 2, 3 }, { 4, 5 }, { 6, 7 },
		{ 0, 2 }, { 1, 3 }, { 4, 6 }, { 5, 7 },
		{ 1, 2 }, { 5, 6 },
		{ 0, 4 }, { 1, 5 }, { 2, 6 }, { 3, 7 },
		{ 2, 4 }, { 3, 5 },
		{ 1, 2 }, { 3, 4 }, { 5, 6 }
	};

	for (size_t i = 0; i < sizeof(idx) / sizeof(idx[0]); ++i) {
		const float x = inout[idx[i][0]];
		const float y = inout[idx[i][1]];

		inout[idx[i][0]] = std::min(x, y);
		inout[idx[i][1]] = std::max(x, y);
	}
}


static void bitonic_scalar_sort(
	float (& inout)[8]) {

	const size_t idx[][2] = {
		{0, 1}, {3, 2}, {4, 5}, {7, 6},
		{0, 2}, {1, 3}, {6, 4}, {7, 5},
		{0, 1}, {2, 3}, {5, 4}, {7, 6},
		{0, 4}, {1, 5}, {2, 6}, {3, 7},
		{0, 2}, {1, 3}, {4, 6}, {5, 7},
		{0, 1}, {2, 3}, {4, 5}, {6, 7}
	};

	for (size_t i = 0; i < sizeof(idx) / sizeof(idx[0]); ++i) {
		const float x = inout[idx[i][0]];
		const float y = inout[idx[i][1]];

		inout[idx[i][0]] = std::min(x, y);
		inout[idx[i][1]] = std::max(x, y);
	}
}


static void insertion_sort(
	float (& inout)[8]) {

	for (size_t i = 1; i < 8; ++i) {
		size_t pos = i;
		const float val = inout[pos];

		while (pos > 0 && val < inout[pos - 1]) {
			inout[pos] = inout[pos - 1];
			--pos;
		}

		inout[pos] = val;
	}
}


static __m128 _nn_movell_ps(
	const __m128 a,
	const __m128 b) {

	return _mm_castpd_ps(_mm_move_sd(_mm_castps_pd(a), _mm_castps_pd(b)));
}

#if __SSE4_1__ == 0
static __m128 _nn_blend_ps(
	const __m128 a,
	const __m128 b,
	const int8_t mask) {

	assert(mask >= 0);
	assert(16 > mask);

	const int32_t u = -1;
	const __m128i vmsk[] = {
		_mm_set_epi32(0, 0, 0, 0),
		_mm_set_epi32(0, 0, 0, u),
		_mm_set_epi32(0, 0, u, 0),
		_mm_set_epi32(0, 0, u, u),
		_mm_set_epi32(0, u, 0, 0),
		_mm_set_epi32(0, u, 0, u),
		_mm_set_epi32(0, u, u, 0),
		_mm_set_epi32(0, u, u, u),
		_mm_set_epi32(u, 0, 0, 0),
		_mm_set_epi32(u, 0, 0, u),
		_mm_set_epi32(u, 0, u, 0),
		_mm_set_epi32(u, 0, u, u),
		_mm_set_epi32(u, u, 0, 0),
		_mm_set_epi32(u, u, 0, u),
		_mm_set_epi32(u, u, u, 0),
		_mm_set_epi32(u, u, u, u)
	};
	const __m128 vmask = _mm_castsi128_ps(vmsk[mask]);

	return _mm_or_ps(_mm_andnot_ps(vmask, a), _mm_and_ps(vmask, b));
}

static __m128 _nn_blendv_ps(
	const __m128 a,
	const __m128 b,
	const __m128 mask) {

	return _mm_or_ps(_mm_andnot_ps(mask, a), _mm_and_ps(mask, b));
}

#else
#define _nn_blend_ps _mm_blend_ps
#define _nn_blendv_ps _mm_blendv_ps

#endif // __SSE4_1__

static void bitonic_simd_sort(
	const float (& input)[8],
	      float (& output)[8]) {

	const __m128 r0_in0 = _mm_load_ps(input + 0); // 0, 1, 2, 3
	const __m128 r0_in1 = _mm_load_ps(input + 4); // 4, 5, 6, 7

	// stage 0
	const __m128 r0_A = _mm_shuffle_ps(r0_in0, r0_in1, 0xcc); // 0, 3, 4, 7
	const __m128 r0_B = _mm_shuffle_ps(r0_in0, r0_in1, 0x99); // 1, 2, 5, 6

	const __m128 r0_min = _mm_min_ps(r0_A, r0_B); // 0, 3, 4, 7
	const __m128 r0_max = _mm_max_ps(r0_A, r0_B); // 1, 2, 5, 6

	// stage 1
	      __m128 r1_A = _mm_shuffle_ps(r0_max, r0_max, 0xf0); // 1, 1, 6, 6
	      __m128 r1_B = _mm_shuffle_ps(r0_max, r0_max, 0xa5); // 2, 2, 5, 5
				 r1_A = _nn_blend_ps(r1_A, r0_min, 0x9);      // 0, 1, 6, 7
			     r1_B = _nn_blend_ps(r1_B, r0_min, 0x6);      // 2, 3, 4, 5

	const __m128 r1_min = _mm_min_ps(r1_A, r1_B); // 0, 1, 6, 7
	const __m128 r1_max = _mm_max_ps(r1_A, r1_B); // 2, 3, 4, 5

	// stage 2
	      __m128 r2_A = _mm_shuffle_ps(r1_max, r1_max, 0xf0); // 2, 2, 5, 5
	      __m128 r2_B = _mm_shuffle_ps(r1_min, r1_min, 0xa5); // 1, 1, 6, 6
				 r2_A = _nn_blend_ps(r2_A, r1_min, 0x9);      // 0, 2, 5, 7
			     r2_B = _nn_blend_ps(r2_B, r1_max, 0x6);      // 1, 3, 4, 6

	const __m128 r2_min = _mm_min_ps(r2_A, r2_B); // 0, 2, 5, 7
	const __m128 r2_max = _mm_max_ps(r2_A, r2_B); // 1, 3, 4, 6

	// stage 3
	const __m128 r3_A = _mm_unpacklo_ps(r2_min, r2_max); // 0, 1, 2, 3
	const __m128 r3_B = _mm_unpackhi_ps(r2_max, r2_min); // 4, 5, 6, 7

	const __m128 r3_min = _mm_min_ps(r3_A, r3_B); // 0, 1, 2, 3
	const __m128 r3_max = _mm_max_ps(r3_A, r3_B); // 4, 5, 6, 7

	// stage 4
	const __m128 r4_A = _mm_movelh_ps(r3_min, r3_max); // 0, 1, 4, 5
	const __m128 r4_B = _mm_movehl_ps(r3_max, r3_min); // 2, 3, 6, 7

	const __m128 r4_min = _mm_min_ps(r4_A, r4_B); // 0, 1, 4, 5
	const __m128 r4_max = _mm_max_ps(r4_A, r4_B); // 2, 3, 6, 7

	// stage 5
	const __m128 r5_a = _mm_unpacklo_ps(r4_min, r4_max); // 0, 2, 1, 3
	const __m128 r5_b = _mm_unpackhi_ps(r4_min, r4_max); // 4, 6, 5, 7
	const __m128 r5_A = _mm_movelh_ps(r5_a, r5_b); // 0, 2, 4, 6
	const __m128 r5_B = _mm_movehl_ps(r5_b, r5_a); // 1, 3, 5, 7

	const __m128 r5_min = _mm_min_ps(r5_A, r5_B); // 0, 2, 4, 6
	const __m128 r5_max = _mm_max_ps(r5_A, r5_B); // 1, 3, 5, 7

	// output
	const __m128 out0 = _mm_unpacklo_ps(r5_min, r5_max); // 0, 1, 2, 3
	const __m128 out1 = _mm_unpackhi_ps(r5_min, r5_max); // 4, 5, 6, 7

	_mm_store_ps(output + 0, out0);
	_mm_store_ps(output + 4, out1);
}


#if __SSE4_1__ != 0
static void bitonik_simd_sort(
	const float (& input)[8],
	      float (& output)[8]) {

	const __m128 r0_in0 = _mm_load_ps(input + 0); // 0, 1, 2, 3
	const __m128 r0_in1 = _mm_load_ps(input + 4); // 4, 5, 6, 7

	// stage 0
	const __m128 r0_A = _mm_shuffle_ps(r0_in0, r0_in1, 0xcc); // 0, 3, 4, 7
	const __m128 r0_B = _mm_shuffle_ps(r0_in0, r0_in1, 0x99); // 1, 2, 5, 6

	const __m128 r0_min = _mm_min_ps(r0_A, r0_B); // 0, 3, 4, 7
	const __m128 r0_max = _mm_max_ps(r0_A, r0_B); // 1, 2, 5, 6

	// stage 1
		  __m128 r1_A = _mm_insert_ps(r0_min, r0_max, 0x10);
		  __m128 r1_B = _mm_insert_ps(r0_min, r0_max, 0x40);
				 r1_A = _mm_insert_ps(r1_A,   r0_max, 0xe0); // 0, 1, 6, 7
			     r1_B = _mm_insert_ps(r1_B,   r0_max, 0xb0); // 2, 3, 4, 5

	const __m128 r1_min = _mm_min_ps(r1_A, r1_B); // 0, 1, 6, 7
	const __m128 r1_max = _mm_max_ps(r1_A, r1_B); // 2, 3, 4, 5

	// stage 2
		  __m128 r2_A = _mm_insert_ps(r1_min, r1_max, 0x10);
		  __m128 r2_B = _mm_insert_ps(r1_max, r1_min, 0x40);
			     r2_A = _mm_insert_ps(r2_A,   r1_max, 0xe0); // 0, 2, 5, 7
				 r2_B = _mm_insert_ps(r2_B,   r1_min, 0xb0); // 1, 3, 4, 6

	const __m128 r2_min = _mm_min_ps(r2_A, r2_B); // 0, 2, 5, 7
	const __m128 r2_max = _mm_max_ps(r2_A, r2_B); // 1, 3, 4, 6

	// stage 3
	const __m128 r3_A = _mm_unpacklo_ps(r2_min, r2_max); // 0, 1, 2, 3
	const __m128 r3_B = _mm_unpackhi_ps(r2_max, r2_min); // 4, 5, 6, 7

	const __m128 r3_min = _mm_min_ps(r3_A, r3_B); // 0, 1, 2, 3
	const __m128 r3_max = _mm_max_ps(r3_A, r3_B); // 4, 5, 6, 7

	// stage 4
	const __m128 r4_A = _mm_movelh_ps(r3_min, r3_max); // 0, 1, 4, 5
	const __m128 r4_B = _mm_movehl_ps(r3_max, r3_min); // 2, 3, 6, 7

	const __m128 r4_min = _mm_min_ps(r4_A, r4_B); // 0, 1, 4, 5
	const __m128 r4_max = _mm_max_ps(r4_A, r4_B); // 2, 3, 6, 7

	// stage 5
	const __m128 r5_a = _mm_unpacklo_ps(r4_min, r4_max); // 0, 2, 1, 3
	const __m128 r5_b = _mm_unpackhi_ps(r4_min, r4_max); // 4, 6, 5, 7
	const __m128 r5_A = _mm_movelh_ps(r5_a, r5_b); // 0, 2, 4, 6
	const __m128 r5_B = _mm_movehl_ps(r5_b, r5_a); // 1, 3, 5, 7

	const __m128 r5_min = _mm_min_ps(r5_A, r5_B); // 0, 2, 4, 6
	const __m128 r5_max = _mm_max_ps(r5_A, r5_B); // 1, 3, 5, 7

	// output
	const __m128 out0 = _mm_unpacklo_ps(r5_min, r5_max); // 0, 1, 2, 3
	const __m128 out1 = _mm_unpackhi_ps(r5_min, r5_max); // 4, 5, 6, 7

	_mm_store_ps(output + 0, out0);
	_mm_store_ps(output + 4, out1);
}

static void odd_even_simd_sort(
	const float (& input)[8],
	      float (& output)[8]) {

	const __m128 r0_in0 = _mm_load_ps(input + 0); // 0, 1, 2, 3
	const __m128 r0_in1 = _mm_load_ps(input + 4); // 4, 5, 6, 7

	// stage 0
#if 0
	const __m128 r0_A = _mm_shuffle_ps(r0_in0, r0_in1, 0x88); // 0, 2, 4, 6
	const __m128 r0_B = _mm_shuffle_ps(r0_in0, r0_in1, 0xdd); // 1, 3, 5, 7

#else
	const __m128 r0_A = r0_in0;
	const __m128 r0_B = r0_in1;

#endif
	const __m128 r0_min = _mm_min_ps(r0_A, r0_B); // 0, 2, 4, 6
	const __m128 r0_max = _mm_max_ps(r0_A, r0_B); // 1, 3, 5, 7

	// stage 1
	const __m128 r1_A = _mm_shuffle_ps(r0_min, r0_max, 0x88); // 0, 4, 1, 5
	const __m128 r1_B = _mm_shuffle_ps(r0_min, r0_max, 0xdd); // 2, 6, 3, 7

	const __m128 r1_min = _mm_min_ps(r1_A, r1_B); // 0, 4, 1, 5
	const __m128 r1_max = _mm_max_ps(r1_A, r1_B); // 2, 6, 3, 7

	// stage 2
	const __m128 r2_A = _mm_movehl_ps(r1_min, r1_min); // 1, 5, -, -
	const __m128 r2_B = r1_max;                        // 2, 6, -, -

	const __m128 r2_min = _mm_min_ps(r2_A, r2_B); // 1, 5, -, -
	const __m128 r2_max = _mm_max_ps(r2_A, r2_B); // 2, 6, -, -

	const __m128 r2_out0 = _mm_movelh_ps(r1_min, r2_min); // 0, 4, 1, 5
	const __m128 r2_out1 = _nn_movell_ps(r1_max, r2_max); // 2, 6, 3, 7

	// stage 3
	const __m128 r3_A = _mm_shuffle_ps(r2_out0, r2_out1, 0x88); // 0, 1, 2, 3
	const __m128 r3_B = _mm_shuffle_ps(r2_out0, r2_out1, 0xdd); // 4, 5, 6, 7

	const __m128 r3_min = _mm_min_ps(r3_A, r3_B); // 0, 1, 2, 3
	const __m128 r3_max = _mm_max_ps(r3_A, r3_B); // 4, 5, 6, 7

	// stage 4
	const __m128 r4_A = _mm_movehl_ps(r3_min, r3_min); // 2, 3, -, -
	const __m128 r4_B = r3_max;                        // 4, 5, -, -

	const __m128 r4_min = _mm_min_ps(r4_A, r4_B); // 2, 3, -, -
	const __m128 r4_max = _mm_max_ps(r4_A, r4_B); // 4, 5, -, -

	const __m128 r4_out0 = _mm_movelh_ps(r3_min, r4_min); // 0, 1, 2, 3
	const __m128 r4_out1 = _nn_movell_ps(r3_max, r4_max); // 4, 5, 6, 7

	// stage 5
	const __m128 r5_A = _mm_insert_ps(r4_out0, r4_out1, 0x60); // 0, 1, 5, 3
	      __m128 r5_B = _mm_insert_ps(r4_out1, r4_out0, 0x90); // 4, 2, 6, 7
	             r5_B = _mm_insert_ps(r5_B,    r4_out1, 0x30); // 4, 2, 6, 4

	const __m128 r5_min = _mm_min_ps(r5_A, r5_B); // 0, 1, 5, 3
	const __m128 r5_max = _mm_max_ps(r5_A, r5_B); // 4, 2, 6, 4

	const __m128 r5_out0 = _mm_insert_ps(r5_min,  r5_max, 0x60);  // 0, 1, 2, 3
	      __m128 r5_out1 = _mm_insert_ps(r5_max,  r5_min, 0x90);  // 4, 5, 6, 4
	             r5_out1 = _mm_insert_ps(r5_out1, r4_out1, 0xf0); // 4, 5, 6, 7

	// output
	_mm_store_ps(output + 0, r5_out0);
	_mm_store_ps(output + 4, r5_out1);
}

#endif // __SSE4_1__

static size_t verify(
	const size_t count,
	float* const input) {

	assert(0 == count % 8);

	for (size_t i = 0; i < count; i += 8)
		for (size_t j = 0; j < 7; ++j)
			if (input[i + j] > input[i + j + 1])
				return i + j;

	return -1;
}

int main(
	int argc,
	char** argv) {

	unsigned alt = 0;
	const bool err = argc > 2 || argc == 2 && 1 != sscanf(argv[1], "%u", &alt);

#if __SSE4_1__ != 0
	if (err || alt > 4 && alt != 100)

#else
	if (err || alt > 2 && alt != 100)

#endif
	{
		std::cerr << "usage: " << argv[0] << " [opt]\n"
			"\t0 bitonic_simd_sort (default)\n"
			"\t1 odd_even_scalar_sort\n"
			"\t2 insertion_sort\n"
#if __SSE4_1__ != 0
			"\t3 bitonik_simd_sort\n"
			"\t4 odd_even_simd_sort\n"

#endif
			"\t100 bitonic_simd_sort, verify consistency of\n"
			<< std::endl;
		return -3;
	}

	if (100 == alt) {
		std::ifstream in("input.sort");

		if (!in.is_open())
			return -1;

		for (size_t i = 0; i < sizeof(input) / sizeof(input[0]); ++i) {
			if (!in.good())
				return -2;

			in >> input[i];
		}

		in.close();

		size_t rep = 8;
		while (rep--) {
			for (size_t i = 0; i < sizeof(input) / sizeof(input[0]); ++i)
				std::cout << input[i] << (i % 8 == 7 ? " ; " : " ");

			std::cout << std::endl;

			for (size_t i = 0; i < sizeof(input) / sizeof(input[0]); i += 8) {
				bitonic_simd_sort(
					*reinterpret_cast< float (*)[8] >(input + i),
					*reinterpret_cast< float (*)[8] >(output + i));
			}

			for (size_t i = 0; i < sizeof(output) / sizeof(output[0]); ++i)
				std::cout << output[i] << (i % 8 == 7 ? " ; " : " ");

			std::cout << std::endl;

			// rotate left each batch of 8
			float carry;

			for (size_t i = 0; i < sizeof(input) / sizeof(input[0]); ++i) {
				if (i % 8 == 0)
					carry = input[i];

				input[i] = (i % 8 == 7) ? carry : input[i + 1];
			}
		}
		return 0;
	}

	const size_t count = 1 << 28;
	float* const input = (float*) malloc(sizeof(float) * count + 63);
	float* const input_aligned = reinterpret_cast< float* >(uintptr_t(input) + 63 & -64);

	std::cout << std::hex << std::setw(8) << input << " (" << input_aligned << ") : " << std::dec << count << " elements" << std::endl;

	for (size_t i = 0; i < count; ++i)
		input_aligned[i] = rand() % 8;

	uint64_t t0;
	uint64_t t1;

	switch (alt) {
	case 0: // test bitonic simd
		{
			t0 = timer_ns();

			for (size_t i = 0; i < count; i += 8) {
				bitonic_simd_sort(
					*reinterpret_cast< float (*)[8] >(input_aligned + i),
					*reinterpret_cast< float (*)[8] >(input_aligned + i));
			}

			t1 = timer_ns();

			const size_t err = verify(count, input_aligned);
			if (-1 != err)
				std::cerr << "error at " << err << std::endl;
		}
		break;

	case 1: // test odd-even
		{
			t0 = timer_ns();

			for (size_t i = 0; i < count; i += 8) {
				odd_even_scalar_sort(
					*reinterpret_cast< float (*)[8] >(input_aligned + i));
			}

			t1 = timer_ns();

			const size_t err = verify(count, input_aligned);
			if (-1 != err)
				std::cerr << "error at " << err << std::endl;
		}
		break;

	case 2: // test insertion
		{
			t0 = timer_ns();

			for (size_t i = 0; i < count; i += 8) {
				insertion_sort(
					*reinterpret_cast< float (*)[8] >(input_aligned + i));
			}

			t1 = timer_ns();

			const size_t err = verify(count, input_aligned);
			if (-1 != err)
				std::cerr << "error at " << err << std::endl;
		}
		break;

#if __SSE4_1__ != 0
	case 3: // test bitonic simd alt
		{
			t0 = timer_ns();

			for (size_t i = 0; i < count; i += 8) {
				bitonik_simd_sort(
					*reinterpret_cast< float (*)[8] >(input_aligned + i),
					*reinterpret_cast< float (*)[8] >(input_aligned + i));
			}

			t1 = timer_ns();

			const size_t err = verify(count, input_aligned);
			if (-1 != err)
				std::cerr << "error at " << err << std::endl;
		}
		break;

	case 4: // test odd-even simd
		{
			t0 = timer_ns();

			for (size_t i = 0; i < count; i += 8) {
				odd_even_simd_sort(
					*reinterpret_cast< float (*)[8] >(input_aligned + i),
					*reinterpret_cast< float (*)[8] >(input_aligned + i));
			}

			t1 = timer_ns();

			const size_t err = verify(count, input_aligned);
			if (-1 != err)
				std::cerr << "error at " << err << std::endl;
		}
		break;

#endif // __SSE4_1__
	}

	const double sec = double(t1 - t0) * 1e-9;
	std::cout << "elapsed time: " << sec << " s" << std::endl;

	free(input);
	return 0;
}

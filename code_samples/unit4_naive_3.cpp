#include <stdint.h>
#include <atomic>

int32_t the_answer;
std::atomic<int32_t> is_ready;

void __attribute__ ((noinline)) deep_thought_compute()
{
	the_answer = 42;
	is_ready = 1;
}

int main(int, char**)
{
	// thread 1
	deep_thought_compute();

	// thread 2
	while (!is_ready)
		asm volatile ("" ::: "memory"); // compiler memory barrier

	return the_answer;
}

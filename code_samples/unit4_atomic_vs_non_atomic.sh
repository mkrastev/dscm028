#!/bin/bash

if [[ $1 == "atomic" ]]; then
	CXXFLAG_ATOMIC=-DATOMIC_FLAG
elif [[ $1 == "nonatomic" ]]; then
	CXXFLAG_ATOMIC=-DATOMIC_FLAG=0
else
	echo Usage: $0 \{atomic\|nonatomic\}
	exit 1
fi

g++ -Ofast -fno-rtti -fno-exceptions -fstrict-aliasing ${CXXFLAG_ATOMIC} -DN_WORKERS=4 unit4_atomic_vs_non_atomic.cpp -o unit4_atomic_vs_non_atomic -lpthread
result=success
for i in $(seq 1000) ; do
	./unit4_atomic_vs_non_atomic >/dev/null 2>&1

	if [ $? -ne 0 ] ; then
		echo error at iter $i
		result=failure
		break
	fi
done
echo $result

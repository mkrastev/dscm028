# DSCM028 Високопроизводителни изчисления

## План на лекциите

* Кратка ретроспекция на компютърните архитектури -- от къде и как произлиза изчислителната производителност
* Конкурентност и паралелизъм -- Flynn's Taxonomy, видове паралелизъм
* Embarrassingly-parallel задачи и такива, за които трябва да се потрудим
* Конкурентност в хардуера, memory consistency и cache coherence
* Roofline моделът за оценяване на производителност
* Event-based profiling -- запознаване с linux perf
* SPMD програмиране -- OpenCL срещу CUDA
* Векторни SIMD архитектури (*пропусната 2020/2021*)
* Разпределени комютри -- OpenMPI (*пропусната 2020/2021*)
* Изчисления с плаваща запетая
* Автовекторизиращи компилатори, generic vectors и intrinsics

## Материали

* [Onur Mutlu, ETH Zurich and Carnegie Mellon University, series of lectures on Computer Architecture](https://www.youtube.com/c/OnurMutluLectures/videos)
* [Herb Sutter, C++ and Beyond 2012: "Atomic Weapons", 1 of 2](https://www.youtube.com/watch?v=A8eCGOqgvH4)
* [Herb Sutter, C++ and Beyond 2012: "Atomic Weapons", 2 of 2](https://www.youtube.com/watch?v=KeLBd2EJLOU)
* [Fedor Pikus, CppCon 2017: “C++ atomics, from basic to advanced. What do they really do?”](https://www.youtube.com/watch?v=ZQFzMfHIxng)
* [The Roofline Model: A Pedagogical Tool for Program Analysis and Optimization, Williams et al., HotChips 2008](https://crd.lbl.gov/assets/pubs_presos/hotchips08-roofline-talk.pdf)
* [The OpenCL Specification ver. 2.1, Khronos Group](https://www.khronos.org/registry/OpenCL/specs/opencl-2.1.pdf)
* [ARMv8-A A64 ISA Overview, Franchin](https://armkeil.blob.core.windows.net/developer/Files/pdf/graphics-and-multimedia/ARMv8_InstructionSetOverview.pdf)
* [A Top-Down Method for Performance Analysis and Counters Architecture, Yasin, ISPASS 2014]()
* A Primer On Memory Consistency and Cache Coherence, Sorib, Hill, Wood, 2011 Morgan &amp; Claypool, ISBN: 9781608455652, available as ebook
* [Shared Memory Consistency Models: A Tutorial, Adve, Gharachorloo](https://www.hpl.hp.com/techreports/Compaq-DEC/WRL-95-7.pdf)
* [Performance Analysis and Tuning of Modern CPUs, Denis Bakhvalov, 2020](https://book.easyperf.net/perf_book)

### Отворена OpenCL имплементация "Portable Computing Language -- PoCL"

[PoCL репо](https://github.com/pocl/pocl)

Упътване за построяване под Debian/Ubuntu (знакът `$` служи само за указание на нов ред -- не се въвежда):
```
$ sudo apt-get install --no-install-recommends libhwloc-dev # this is optional as pocl 1.5 has alternative means to collect hw info other than libhwloc, but it's still recommended
$ sudo apt-get install build-essential llvm-8-dev llvm-8 clang-8 libclang-8-dev libpng-dev cmake pkg-config
$ git clone https://github.com/pocl/pocl.git
$ mkdir pocl_build
$ cd pocl_build
$ cmake ../pocl -DDEFAULT_ENABLE_ICD=0 -DWITH_LLVM_CONFIG=/usr/bin/llvm-config-8
$ make -j4
$ sudo make install
```
Така построеният libOpenCL.so се намира на `/usr/local/lib`, което означава, че вероятно не е включен в стандартните пътища за тъсене на библиотеки, и затова при употреба указваме изрично локацията по следния начин:
```
$ LD_LIBRARY_PATH=/usr/local/lib/ ./test_cl # an arbitrary app utilizing libOpenCL.so
```

### 1. Кратка ретроспекция на компютърните архитектури -- от къде и как произлиза изчислителната производителност

* [Von Neumann architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture)
    * [Extended version](https://en.wikipedia.org/wiki/Computer_architecture)
    * [The iron law of performance](https://en.wikipedia.org/wiki/Joel_Emer)
        * [explanation](images/iron_law_performance.png)
* [The bottleneck that grew into a wall](https://en.wikipedia.org/wiki/Random-access_memory#Memory_wall)
    * [Energy costs of moving a data bit](images/cs184.eecs.berkeley.edu_slide-11.jpg)
        * [source](https://cs184.eecs.berkeley.edu/public/sp19/lectures/lec-24-high-performance-image-processin/slides/slide-11.jpg)
    * [Time costs of moving a data bit](images/computationstructures.org_Slide15.png)
        * [source](https://computationstructures.org/lectures/caches/caches.html)
* [Moore's law & Dennard scaling](https://www.youtube.com/watch?v=_nnRNwgENss)
    * [Decades of microprocessor trend data](https://www.karlrupp.net/2015/06/40-years-of-microprocessor-trend-data/)
    * [35 years of processor trend](images/35-years-processor-trend.png)
    * [40 years of processor trend](images/40-years-processor-trend.png)

Допълнителени материали:

* [Efficiency and Parallelism: The Challenges of Future Computing by William Dally](https://www.youtube.com/watch?v=l1ImS3gbg08)
* [Moore's Law: Where are we and which way are we going? by Greg Yeric | ARM TechCon 2016](https://www.youtube.com/watch?v=MuTfMY9kuHg)
* [Seminar in Computer Architecture - Meeting 1: Introduction (ETH Zürich, Fall 2019)](https://www.youtube.com/watch?v=Rh5XynlJrSM)

### 2. Конкурентност и паралелизъм -- Flynn's Taxonomy, видове паралелизъм

* Конкурентност и паралелизъм
	* Какво е конкурентност
	* Чрез конкурентност към паралелизъм
* Видове паралелизъм -- общa терминология
	* На ниво размер на машинната дума -- Bit-level Parallelism (**BLP**)
		* Архаизъм
	* На ниво подзадачи във Фон-Ноймановото изпълнение на потока инструкции
		* Конвейрнизация на потока -- [**Instruction pipelining**](https://en.wikipedia.org/wiki/Instruction_pipelining)
	* На ниво конкурентност на инструкциите и ширина на конвейра за инструкции -- Instruction-level Parallelism (**ILP**)
		* Суперскаларност срещу VLIW
		* SMT
	* На ниво конкурентност при хомогенни данни -- Data-level parallelism (**DLP**)
	* На ниво конкурентност при (под)задачите -- Task-level parallelism (**TLP**)
		* Многонишковост
		* Многозадачност
		* Разпределеност
	* На ниво достъп до паметта -- Memory-level parallelism (**MLP**)
* [Класификация на Флин](https://en.wikipedia.org/wiki/Flynn%27s_taxonomy)
	* Single Instruction, Single Data (**SISD**)
	* Single Instruction, Multiple Data (**SIMD**)
	* Multiple Instructions, Single Data (**MISD**)
	* Multiple Instructions, Multiple Data (**MIMD**)
* Парадигми за програмиране, стъпващи на DLP
	* [Векторни процесори, възходът на Сиймур Крей](https://en.wikipedia.org/wiki/Seymour_Cray)
		* Програмиране на SIMD архитектури -- описване на задачата в ширина
		* Програмиране на SIMT архитектури -- описване на задачата в дълбочина
	* Single Program, Multiple Data (**SPMD**) парадигма за описване в дълбочина
	* Автовекторизация и [OpenMP](https://en.wikipedia.org/wiki/OpenMP)
* Панацея ли е паралелизмът?
	* [Законът на Амдал](https://en.wikipedia.org/wiki/Amdahl%27s_law)
	* [Законът на Густафсон-Барси](https://en.wikipedia.org/wiki/Gustafson%27s_law)

Допълнителни материали:

* [Паралелизъм и data locality - основи за висока производителност | HackConf 2017](https://www.youtube.com/watch?v=y1_oGLiezcU)
* [*Are We Ready for High Memory-Level Parallelism?* Ceze, Tuck, Torrellas](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.136.2794&rep=rep1&type=pdf)
* [*MLP yes! ILP no!* Glew](https://people.eecs.berkeley.edu/~kubitron/asplos98/abstracts/andrew_glew.pdf)

### 3. Embarrassingly-parallel задачи и такива, за които трябва да се потрудим

* Засрамващо-паралелни задачи, или когато конкурентността ни бие по челото
	* Обработка на масиви от данни чрез мапинг функции
		* Компютърна графика -- примитиво-проекционна и лъчетрасираща
		* Физически симулации -- твърдотелни, флуидни, произволни
		* Транспониране на таблици, SELECT върху бази данни
		* Обработка на сигнали (DSP)
	* Обработка на масиви от данни чрез редукционни функции -- характеризиращи функции
		* Криптография -- верификация и подписване на данни
		* Конволюционни трансформации на масиви -- невронни мрежи
* Итеративно-рекурентни задачи, или когато трябва да се потрудим за паралелизма
	* Общо правило за изчислителната сложност при [редукции с асоциативни оператори](slides/unit3_reductions.pdf)
* Два примера на на-пръв-поглед последователни задачи, които имат фундаментален паралелни реализации
	* Сортиращи мрежи -- [bitonic & odd-even sort](slides/unit3_sorting_networks.pdf)
	* Префиксни суми -- prefix sum, AKA array scan

Допълнителен материал:

* [GPU Gems 3: Parallel Prefix Sum (Scan) with CUDA](https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-39-parallel-prefix-sum-scan-cuda)
* [Генератор на сортиращи мрежи](https://pages.ripco.net/~jgamble/nw.html)

### 4. Конкурентност в хардуера, memory consistency и cache coherence -- част 1

* В търсене на все повече суперскаларност (ILP) -- за експоненциално-скъпите неща в природата, или как да си набавим конкурентност дори когато я няма
	* Архитектурно (или още **програмно-видимото**) състояние на Фон-Ноймановия компютър срещу не-архитектурното (или **микроархитектурното**) му състояние
		* Фундаментално свойство на Фон-Ноймановия комютър -- прекъсваемост на потока инструкции
		* Състояние на регистровия файл (за дадено ниво на привилигированост)
		* Състояние на един адрес в личната или споделена памет на една нишка -- [cache coherence](https://en.wikipedia.org/wiki/Cache_coherence)
		* Състояние на множество активни адреси в споделената памет -- [memory consistency](https://en.wikipedia.org/wiki/Consistency_model)
	* Фон-Нойманови процесори, изпълняващи инструкциите в реда, в който са описани -- In-order pipeline
		* При липса на cross-instruction dependencies в потока, и наличието на достатъчно широк конвейр, съседни инструкции могат да се изпълнят едновременно
			* Компилаторът **може** да преподреди потока в по-благопирятен вид -- избягвайки зависимости между съседни инструкции
		* Основният принцип на VLIW архитектурите -- компилаторът **е длъжен** да подреди потока избягвайки зависимости между инструкциите в една дълга дума
			* Практически непосилна задача за изпълнение по време на компилация (**at compile-time**)
		* Предварителна (Ahead-of-time) компилация и такава в последния момент (Just-in-time, AKA JIT компилация)
	* Фон-Нойманови процесори, изпълняващи инструкциите **извън** реда, в който са описани -- Out-of-order pipeline
		* Гъвкаво решение, базирано на анализиране на потока инструкции по време на изпълнение (**at run-time**) и отново достатъчно широк конвейр
			* Много повече конкуретност ако не се ограничаваме до съседни инструкции
			* Правило: всяка инструцкия може да се изпълни в момента, в който са налични операндите ѝ -- [алгоритъм на Томасуло](https://en.wikipedia.org/wiki/Tomasulo_algorithm)
				* Някои инструкции дори по-рано: спекулативно изпълнение на инструкции
	* Повече суперскаларност == повече спекулативност -- или какво изкопали джуждетата от недрата на Мория
		* Много спекулативност: branch predictors, branch targets buffers, cache prefetchers
		* Събудили демона на спекулативността
			* [Meltdown](https://en.wikipedia.org/wiki/Meltdown_(security_vulnerability))
			* [Spectre](https://en.wikipedia.org/wiki/Spectre_(security_vulnerability))
		* А сега накъде?

Допълнителен материал:

* [Dynamic Scheduling Using Tomasulo's Algorithm, Part 1, Prof. Dr. Ben H. Juurlink, Technische Universität Berlin](https://www.youtube.com/watch?v=y-N0Dsc9LmU)
* [Dynamic Scheduling Using Tomasulo's Algorithm, Part 2, Prof. Dr. Ben H. Juurlink, Technische Universität Berlin](https://www.youtube.com/watch?v=YH2fFu-35L8)
* [Dynamic Scheduling Using Tomasulo's Algorithm, Part 3, Prof. Dr. Ben H. Juurlink, Technische Universität Berlin](https://www.youtube.com/watch?v=Zwh3VKU1d_o)
* [Robert Tomasulo speaking at UMich](https://www.youtube.com/watch?v=S6weTM1tNzQ)

### 4. Конкурентност в хардуера, memory consistency и cache coherence -- част 2

* Концепцията за страничен ефект върху споделен ресурс -- що е **функция** и какво е **състояние**?
* Memory consistency model, или какви са импликациите от това, че можем да преподреждаме инструкции както ни изнася?
	* Забавен пример -- [прекалена конкурентност?](https://godbolt.org/z/xWaaK3)
	* Кой може да преподрежда кода ни?
		* Компилаторът
		* Процесорът
		* Други агенти по пътя до споделени ресурси като централната памет, напр контролери на паметта
	* Какви формални гаранции за подредба ни дават езиците?
		* Какво въобще представлява редът? -- [sequence points](https://en.wikipedia.org/wiki/Sequence_point)
		* Подредба на обръщения към паметта -- [memory ordering](https://en.wikipedia.org/wiki/Memory_ordering)
		* Java/C/C++ Sequential Consistency -- [an overview](https://www.modernescpp.com/index.php/sequential-consistency)
			* Atomicity and lack thereof
			* Barriers
			* Acquire-Release semantics -- [C/C++ memory_order](slides/unit4_c++_memory_ordering.pdf)
* Cache coherence, или въпросът за универсалната истина през погледа на Фон-Ноймановия компютър
	* CPU cache -- типичен агент по пътя до споделения ресурс 'централна памет'

Допълнителен материал:

* [Herb Sutter, C++ and Beyond 2012: "Atomic Weapons", 1 of 2](https://www.youtube.com/watch?v=A8eCGOqgvH4)
* [Herb Sutter, C++ and Beyond 2012: "Atomic Weapons", 2 of 2](https://www.youtube.com/watch?v=KeLBd2EJLOU)
* [Fedor Pikus, CppCon 2017: “C++ atomics, from basic to advanced. What do they really do?”](https://www.youtube.com/watch?v=ZQFzMfHIxng)
* A Primer On Memory Consistency and Cache Coherence, Sorib, Hill, Wood, 2011 Morgan &amp; Claypool, ISBN: 9781608455652, available as ebook

### 5. Roofline моделът за оценяване на производителност

* Концепцията за [асимптотичен модел за оценяване на производителност въз основа на лимитите на хардуера](https://en.wikipedia.org/wiki/Roofline_model)
	* Лимитът 'пропускливост на паметта' -- масимални bytes/s
	* Лимитът 'изчислителен ресурс' -- максимални Floating Point Ops per Second (FLOPS)
	* Концепцията 'изчислителен интензитет' -- отношение между FLOPS и байтове/s -> FLOP/byte
* генерализация за релевантни мерни единици, базирани на време
	* Пропускливост на паметта може също да бъде
		* обръщения за единица време
		* обръщения за процесорен такт
	* Операции за секунда може също да бъде
		* операции за процесорен такт
* пример -- [General Matrix Multiplication (GEMM)](https://bitbucket.org/mkrastev/gemm/src/master/)

Допълнителен материал:

* [Performance Analysis and Tuning of Modern CPUs, Denis Bakhvalov, 2020, Chapter 5.5 Roofline Performance Model](https://book.easyperf.net/perf_book)

### 6. Linux perf

* Каква информация ни дават профилаторите?
	* Колко време е отнела и/или интересни-за-производителността събития са се случили в коя част от кода
	* Коя част от кода какво е причинила/извикала -- call graphs
* Фундаментални [видове профилатори](https://en.wikipedia.org/wiki/Profiling_(computer_programming))
	* Време-прекъсващи -- семплират на кое място в кода е прекъснатия процес, и създават картина на процент прекарано време по функция/израз
		* Не особено надежни в наши дни; дават абсолютния минимум информация за профилиране, често недостатъчна за ефикасна работа
	* Инструментиращи -- инжектират маркери най-често по време на строенето на кода, и следят за натрупаните времена и/или събития
		* Могат да бъдат прекалено инвазивни и да изкривят получената картина
	* Интерпретиращи -- симулират изпънението на кода на ниво инструкция
		* Безкрайно бавни за реален софтуер
	* Event-based sampling (EBS) -- отброяват събития, генерирани от специализирани апаратни средства -- Performance Monitoring Unit (**PMU**) в процесора
		* Типовете събития могат да бъдат дестки или дори стотици, в зависимост от микроархитектурата
		* Събитията се отброяват от апаратни броячи -- Performance Monitoring Counters (**PMC**), които са програмируем но силно лимитиран ресурс
		* Днешният стандарт
* Linux perf
	* Работата по следене на процеса/ите се случва в кърнела, от потребителска страна се ползва чрез помощната програма (utility) perf
		* Можете да си напишете собствен профилатор, който да ползва същите услуги на кърнела!
	* Основен интерес за нас представляват
		* perf list
		* perf stat
		* perf record
		* perf report
* пример -- [General Matrix Multiplication (GEMM)](https://bitbucket.org/mkrastev/gemm/src/master/)

Допълнителен материал:

* [Performance Analysis and Tuning of Modern CPUs, Denis Bakhvalov, 2020, Chapter 3.9 Performance Monitoring Unit](https://book.easyperf.net/perf_book)

### 7. OpenCL (OCL)

* API за пълноценното възползване от DLP в една задача, независимо какъв тип процесор(и) имаме на разположение -- SISD, SIMD, MIMD, или комбинация от тях
* Клиентска (host) страна и изпълнителна/изчислителна (device) страна
* Напълно thread-safe клиентска страна
	* Няма концепция за водеща/главна клиентска нишка -- всички клиентски нишки могат да ползват всички API функции без изрична синхронизация
* Reference-counted модел за продължителност на живота на обектите
* SPMD модел за програмиране на работните процесори -- OCL 'устройства' (devices), изпълняващи SPMD 'ядра' (kernels)
* Интуитивен модел на представяне на задачите
	* Една задача се определя като пускане на едно ядро върху многомерен масив (grid) -- от 1 до 3 измерения
		* Броят позиции във всяко едно измерение е ограничен според драйвера/хардуера
	* Всяка една позиция в грида представлява 'работна единица', която ще се обработи от една работна нишка, без гаранции в какъв ред и точно кога -- пълна конкурентност
	* Всяко едно OCL устройство във есеки един момент се занимава с една или повече 'работни групи' от работни единици
		* Работната група е многомерно подмножество с размери дефинирани от потребителя, изпълняващо се на едно 'изчислително устройство' (Compute Unit -- CU)
		* OCL устройства с множество CU обработват съответния брой работни групи във всеки един момент
		* Подмножествата на работните групи нямат припокритие
			* Всяка работна единица от грида ще се обработи от една единствена нишка в една единствена работна група, в неуточнен момент
		* Потребителят не определя в кой момент кое CU къде се намира като подмножество в глобалното пространство на задачата, *но..*
		* Работната група има пълната представа къде е 'изпратена' да работи -- интроспекция на ниво нишка в групата и нишка в грида
	* Къде е уловката?
		* Адресното пространсво на клиентския процес няма нищо общо с адресното пространсво на OCL устройствата
		* Те на свой ред имат едно глобално и отделни локални адресни пространства за всяко CU
			* *A pointer to host mem is not a pointer to device mem, is not a pointer to CU local mem*
		* Засега нещата с адресните пространства стоят така, но тенденцията е да отпадне разделението хост/устройство
* [Кратко сравнение между OpenCL и CUDA](slides/unit7_intro_gpgpu_apis.pdf)
* пример -- [opencl caps viewer](https://bitbucket.org/mkrastev/test-cl/)
* пример -- [raycasting](https://github.com/ChaosGroup/cg2_2014_demo/tree/master/prob_7)

Допълнителен материал:

* [OpenCL 1.2 API and C Language Specification](https://www.khronos.org/registry/OpenCL/specs/opencl-1.2.pdf)
* [OpenCL 1.2 Quick Reference Card](https://www.khronos.org/registry/OpenCL//sdk/1.2/docs/OpenCL-1.2-refcard.pdf)
* [OpenCL 1.2 Reference Pages](https://www.khronos.org/registry/OpenCL//sdk/1.2/docs/man/xhtml/)

### 8. Операции с плаваща запетая

* Историческите корени на машинните изчисления с плаваща запетая
	* Първи компютри, подържащи плаваща запетая - 1938
	* Пръв общоприет стандар за плаваща запетая - IEEE-754-1985
* Нуждата от представяне на дробни числа
	* Изцяло позиционни формати -- двоични дроби -- двоична дробна запетая
	* Формати с фиксирана запетая -- 'модификация' върху целите числа
	* Имплицитно (подразбиращо се) разделение на цяла и дробна част -- фиксиран брой битове за всяка
		* Двоичната точка е винаги на предварително-установена битова позиция
	* Пример с FX4.4
		* Загуба на точност при умножение
* Идеята на числата с плаваща запетая
	* Броят значещи битове отново е фиксиран, *но..*
	* Двоичната точка вече може да бъде на (огромно) множество позиции спрямо значещите битове, специфична за всяко записано число
		* Всяко число носи информация къде е двоичната му точка
	* Пример с FP8
		* Загуба на точност при .. събиране
	* Основни характеристики на числата с плаваща запетая
		* Неравномерно разпледеление върху числовата ос -- равномерно само между съседни степени на базата (piece-wise linear; large-scale exponential)
		* Изключително мощно средство при експоненциални функции
		* Не-асоциативност при събиране -- (a + b) + c =/= a + (b + c)
* Как да се справим с този малък проблем
	* Увеличаване на броя значещи битове -- fp32 -> fp64
	* Сортиране на събираемите по величина
	* Интелигентни алгоритми за сумиране на числа с плаваща запетая -- Сумиране на Кан
* Кой е проф. Уилям Кан?
	* Алгоритъм за сумиране на числа с плаваща запетая, с проследяване на натрупаната грешка
* Разглеждане на стандарта IEEE-754
* Пример за намиране на ULP разстояние между две произволни числа с плаваща запетая

Допълнителен материал:

* [За плаваемостта на плаващата запетая](slides/unit8_on_the_buoyancy_of_the_fp.pdf)

### 9. Автовекторизиращи компилатори, generic vectors и intrinsics

* Що е то 'оптимизиращ компилатор'?
	* Компилатор, който може да приложи произволни трансформации спрямо програмния ни код, с цел да се подобри скоростта и/или размера на изходния код, запазвайки оригиналното поведение
	* Освен ако..
		* Бъгове в компилатора
		* Пекалено агресивни оптимизации, на които компилиращият кода се е съгласил
			* Компилиращият кода разбира ли какво прави?
	* В наши дни е несериозно един компилатор да не е оптимизиращ, и като цяло такива не се срещат извън експеримента
	* В C/C++ света, където производителността е на особена почит, всички компилатори са оптимизиращи, но не задължително по еднакъв начин/с еднакъв успех
* Различаваме два основни прийома
	* Ahead-of-time (**AOT**) vs Just-in-time (**JIT**)
	* AOT е *ранна* компилация, при която малко е известно за контекста на извикване на компилирания код
		* Компилаторът има цялото време на света да си свърши работата
	* JIT е *максимално късна* компилация, при която е известно значително повече за контекста на извиквания код
		* Максимално-късно == at runtime -- няма много време за бавене при компилация
		* А не можем ли да изпъним програмта 'неофициално', без да ни е грижа за времето за изпълнение?
		* Можем, разбира се -- тогава JIT се превръща в Profile-based optimizations -- бавна AOT компилация но с контекста от 'неофициалния' runtime
			* ..И все пак, колко точно 'неофициален' може да бъде нашия неофициалне runtime?
* Основни похвати на оптимизиращите компилатори
	* Dead code elimination -- премахване на ненужни функции/изрази
	* Специализиране на генеричен код за мястото на приложение
		* Function inlining
		* Partial/total evaluation
			* Expression folding
			* Constant propagation
	* Expression reordering
		* Code generation за конкретна микроархитектура -- instruction scheduling
	* Memory-access elimination heuristics -- елиминиране на обръщения към паметта разчитайки на по-ранни копия на прочетените данни
	* Експлоатация на DLP за генериране на SIMD код -- autovectorization
* Най-добрата оптимизация винаги е в резултат на екипна работа между езика, програмиста и компилатора
	* Intrinsics -- 'вградени' функции от езика или от системни библиотеки към него, гарантиращи оптимално поведение
	* Generic-vector datatype extensions, значително улесняващи автовекторизацията
		* Защо това не е стандарна функционалност на езика?
	* OpenMP auto-multithreading (TLP)

Допълнителен материал:

* [Efficient vectorization of C/C++](slides/unit9_efficient_vectorization.pdf)
* [DLP at low SIMD prices](https://bitbucket.org/mkrastev/gemm/src/master/DLP_at_low_SIMD_prices.pdf)
